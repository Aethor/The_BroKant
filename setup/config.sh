#!/bin/bash

echo "Welcome to this Fedora auto configuration"

echo "Updating..."
sudo dnf -y update

echo "Creating ~/Dev directory..."
mkdir -p ~/Dev

echo "Enabling RPM Fusion..."
sudo dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

echo "Do you want to install nvidia proprietary driver from RPM fusion ?"
select answer in "yes" "no"; do
    case $REPLY in
	yes) sudo dnf -y install akmod-nvidia; break;;
	no) echo "nvidia driver were not installed"; break;;
    esac
done

echo "Installing git..."
sudo dnf -y install git
echo "Manual configuration of git"
git config --global --edit

echo "Installing Vim..."
sudo dnf -y install vim-enhanced

echo "Installing latest Emacs..."
sudo dnf -y group install "Development Tools"
sudo dnf -y builddep emacs
sudo dnf -y install jansson jansson-devel
cd ~/Dev
git clone -b master https://github.com/emacs-mirror/emacs.git
cd emacs
./autogen.sh
./configure
make
sudo make install


echo "Installing fish..."
sudo dnf -y install fish
chsh -s /usr/bin/fish 
touch ~/.config/fish/config.fish 

echo "Installing telegram-desktop..."
sudo dnf -y install telegram-desktop

echo "Setting up gitlab..."
sudo dnf -y install xclip
ssh-keygen -t rsa -C "oow.autre@gmail.com" -b 4096
xclip -sel clip < ~/.ssh/id_rsa.pub
echo "A new SSH key has been created and copied to the clipboard. Please enter it on gitlab.com"
read -p "When done, press enter"
cd ~/Dev
git clone git@gitlab.com:Aethor/The_BroKant.git

echo "Importing personal settings..."
cd ~/Dev/The_BroKant/dotfiles/
./dotfiles/dotfiles.sh --update-computer

echo "Installing pass..."
echo "Please import your private key before gopass installation"
echo "Hint : gpg --import path_to_private_key"
read -p "When done, press enter"
sudo dnf -y install pass
git clone git@gitlab.com:Aethor/passs.git ~/.password-store/

echo "Installing browserpass"
sudo dnf -y install golang
cd ~/Dev
git clone https://github.com/browserpass/browserpass-native
cd browserpass-native
make configure
make
sudo make install
cd /usr/lib/browserpass/
make hosts-firefox-user
echo "Don't forget to install the browserpass extension (https://addons.mozilla.org/en-US/firefox/addon/browserpass-ce/)"

cd ~
echo "Configuration finished !"
echo -e "You might want to reboot if\n1) kernel was updated\n2) akmod nvidia drivers were installed"
