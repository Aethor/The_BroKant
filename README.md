# Welcome to the BroKant !

This is the BroKant, where I keep useful things for me

List of everything there :

* wiki : the almighty wiki
* setup : a shell script to setup my Fedora install
* old : old little helper projects. All of them are probably irrelevants, but I keep them for the nostalgia
  * Absurage : a little script to help my father ;)
  * Project BirL.sh : A bash script to transform Markdown Lombardi + LaTeX into PDF. Other things come with it : a Markdown Lombardi vim syntax file ( with basic LaTex support ) : mdlbi.vim , a file with Markdown Lombardi commands, a vim plugin (vimBirL) and an install script which should set up everything fine ( if you have pathogen for vim ). If you're wondering, Markdown Lombardi was my own (bad) version of markdown at the time.
  * ProjectP : Was aiming to do a upc, c and c++ project template generator
  * Spillow.sh : A bash script to convert C into french standard algorithm language.
  * Algo.vim : My syntax file for french algorithms : nothing fancy, but great to use with my Vim config.
  * Vim settings : My vimrc ( windows and linux versions ), some vim colorscheme I made ( NinjaNight, Aethor, AethorII ).

