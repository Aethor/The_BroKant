#!/usr/bin/bash

if [[ $1 = "--update-brokant" ]];then
	cp ~/.vimrc ./vimrc
	cp ~/.emacs ./emacs
	cp ~/.emacs.d/init.org ./init.org 
	cp ~/.config/gtk-3.0/gtk.css ./gtk.css
	cp ~/.mbsyncrc mbsynrc
	cp ~/.msmtprc msmtprc 
elif [[ $1 = "--update-computer" ]];then
	cp ./vimrc ~/.vimrc
	cp ./emacs ~/.emacs
	cp ./init.org ~/.emacs.d/init.org 
	cp ./gtk.css ~/.config/gtk-3.0/gtk.css
	cp ./mbsynrc ~/.mbsyncrc
	cp ./msmtprc ~/.msmtprc 
elif [[ $1 = "-h" ]];then
	echo "--update-brokant : update repo"
	echo "--update-computer : update computer"
else
	echo "unrecognized command"
fi
