;;; mariana.el --- A port of Sublime Text's Mariana
;;
;; Author: Arthur Amalvy
;; Version: 0.0.1
;; Package-Version: 
;; Package-Commit: 
;; Keywords: themes
;; URL: 
;;
;; This file is not part of GNU Emacs.
;;
;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.
;;
;;; Code:

(deftheme mariana
  "A port of Sublime Text's Mariana")

(let ((black "#000000")
      (blue "#6699cc")
      (blue-vibrant "#5c99d6")
      (blue2 "#596673")
      (blue3 "#303841")
      (blue4 "#647382")
      (blue5 "#5fb4b4")
      (blue6 "#a6acb9")
      (green "#99c794")
      (grey "#333333")
      (orange "#f9ae58")
      (orange2 "#ee932b")
      (orange3 "#fac761")
      (pink "#c695c6")
      (red "#ec5f66")
      (red2 "#f97b58")
      (white "#ffffff")
      (white2 "#f7f7f7")
      (white3 "#d8dee9"))
  (custom-theme-set-faces
   'mariana

   ;; Basics
   `(default ((t (:background ,blue3 :foreground ,white))))
   `(cursor ((t (:foreground ,orange :background ,white))))
   `(highlight ((t (:background ,blue5 :foreground ,white))))
   `(hl-line ((t (:background ,blue2))))
   `(region ((t (:background ,blue2))))
   `(escape-glyph ((t (:foreground ,pink))))
   `(minibuffer-prompt ((t (:foreground ,blue2)))) ;todo

   ;; Font-lock stuff
   `(font-lock-builtin-face ((t (:foreground ,red))))
   `(font-lock-constant-face ((t (:foreground ,red))))
   `(font-lock-comment-face ((t (:italic t :foreground ,blue6))))
   `(font-lock-doc-face ((t (:foreground ,blue6))))
   `(font-lock-doc-string-face ((t (:foreground ,blue6))))
   `(font-lock-function-name-face ((t (:foreground ,blue))))
   `(font-lock-function ((t (:foreground ,blue))))
   `(font-lock-keyword-face ((t (:foreground ,pink))))
   `(font-lock-negation-char-face ((t (:foreground ,red))))
   `(font-lock-preprocessor-face ((t (:foreground ,blue))))
   `(font-lock-string-face ((t (:foreground ,green))))
   `(font-lock-type-face ((t (:bold t :foreground ,blue))))
   `(font-lock-variable-name-face ((t (:foreground ,orange))))
   `(font-lock-warning-face ((t (:background ,red :foreground "white"))))

   ;; UI related
   `(link ((t (:foreground ,blue2))))
   `(button ((t (:foreground ,blue2 :background ,blue :weight bold :underline t))))
   `(mode-line ((t (:background ,blue2 :foreground ,white))))
   `(mode-line-inactive ((t (:background ,grey :foreground ,white))))
   `(vertical-border ((t (:foreground ,black))))
   `(fringe ((t (:background ,grey :foreground ,white))))

   ;; Linum
   `(linum ((t (:background ,grey :foreground ,white))))

   ;; show-paren
   `(show-paren-match ((t (:background ,orange))))
   `(show-paren-mismatch ((t (:inherit font-lock-warning-face))))

   ;; flyspell-mode
   `(flyspell-incorrect ((t (:underline ,red))))
   `(flyspell-duplicate ((t (:underline ,orange))))

   ;; magit
   `(magit-diff-add ((t (:foreground ,green))))
   `(magit-diff-del ((t (:foreground ,red))))
   `(magit-item-highlight ((t (:background ,blue2))))))


;;;###autoload
(and load-file-name
  (boundp 'custom-theme-load-path)
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'mariana)

;;; mariana.el ends here
