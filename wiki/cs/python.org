#+TITLE: Python
#+AUTHOR: Arthur Amalvy


* Typing

** Refer to current class with typing

#+begin_src python
from __future__ import annotations
#+end_src
   
*** References

- [[https://stackoverflow.com/questions/15853469/putting-current-class-as-return-type-annotation][Stackoverflow answer]]

* Argparse

** Getting a dictionary from the command line

It is possible to get a dictionary from the command line with no
effort with argparse using a "hack" with the json module :

#+begin_src python
import argparse, json

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--dictionary", type=json.loads)
args = parser.parse_args()

args.dictionary
#+end_src

At the command line, the passed dictionary must be in JSON
format. Example call :

#+begin_src sh
python script.py --dictionary '{"a": 1, "b": 2}'
#+end_src

* Printing exceptions tracebacks

#+begin_src python
import traceback

try:
    raise Exception("!")
except Exception:
    traceback.print_exc()
#+end_src

* PDB

** Continue over a ~pdb.set_trace()~

If you used ~pdb.set_trace()~ to set a breakpoint in your program, it
cant be listed with =break= nor deleted with =clear=. Instead, you can
use this hack to unassign the ~set_trace~ function :

#+begin_src text
(pdb) pdb.set_trace = lambda: None
#+end_src

* Getting os type

[[https://docs.python.org/3/library/sys.html#sys.platform][Reference]]

#+begin_src python
import sys

if sys.platform.startswith("linux"):
    # linux related code
    pass
elif sys.platform == "darwin":
    # mac related code
    pass
elif sys.platform == "win32":
    # windows related code
    pass
else:
    raise ValueError(f"unknown OS : {sys.platform}")
#+end_src

* F-strings

** Float formatting using f-strings

Format specifiers can be used in f-strings (see [[https://docs.python.org/3/library/string.html#format-specification-mini-language][here]] for a format
specifier reference). As an example, to keep only 2 digits after the
decimal point :

#+begin_src python
some = 1.3209409348
print(f"{some:.2f}")
#+end_src

* Sphinx

- [[https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html][directives list]]

** Quickstart

- Install Sphinx (=pip install sphinx=)
- Create and go to a directory that will contain your doc. Use
  =sphinx-quickstart=.
- Several files were created :
  - =index.rst= is the main documentation document
  - =conf.py= controls sphinx behaviour. It can be used to add
    extensions.
- =make html= generate the doc as HTML under the build directory.
- If git (or other version control) is used, you probably want to
  ignore the =_build= or =build= folder.

*** References

- [[https://www.sphinx-doc.org/en/master/usage/quickstart.html][Sphinx Quickstart Documentation]]

** Extensions

Extensions can be added in the =conf.py= file. You can add the extension name in the =extensions= list.

*** Autodoc

- Add ="sphinx.ext.autodoc"= to the =extensions= list
- To be able to import modules when creating the documentation, add
  =sys.path.insert(0, os.path.abspath(${path to module}))= to the
  beginning of =conf.py=

**** References

[[https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html][Sphinx Documentation]]


*** Autodoc Typehints

- Install the python package : =pip install sphinx-autodoc-typehints=
- Add ="sphinx_autodoc_typehints"= to the =extensions= list

**** Interesting options

All options can be set as variables in =conf.py=
- set =always_document_param_types= to =True= to have type
  automatically documented with type hints (otherwise, it will only
  work for parameters specified in the docstring)


**** References

[[https://pypi.org/project/sphinx-autodoc-typehints/][PyPi page]]


*** Read the docs theme

- Install the python package : =pip install sphinx-rtd-theme=
- Add ="sphinx_rtd_theme"= to the =extensions= list.
- Change =html_theme= to ="sphinx_rtd_theme"=

**** References

- [[https://sphinx-rtd-theme.readthedocs.io/en/stable/][Sphinx ReadTheDocs Documentation]]


** Gitlab CI

Create a .gitlab-ci.yml in your repository

*** Example

here, =./docs/= is the doc directory. Plugins you use must be
installed before building, see the first line in the =script' entry
here.

#+begin_src yaml
image: python:3.7-alpine

pages:
  stage: deploy
  script:
    - pip install -U sphinx sphinx-autodoc-typehints sphinx-rtd-theme
    - sphinx-build -b html ./docs/ public
    artifacts:
      paths:
        - public
        only:
          - master
#+end_src

*** Autodoc issues

Some modules won't be importable by autodoc on gitlab (unless you
install them...). To counteract that, the =autodoc_mock_imports= list
can be set in =conf.py=. Example :

#+begin_src python
autodoc_mock_imports = ["torch", "transformers", "numpy", "tqdm"]
#+end_src


** Github CI

See [[https://tomasfarias.dev/posts/sphinx-docs-with-poetry-and-github-pages/][This blog post]].

create a =.yml= ending file in =.github/workflows=. Here is an example
of ci file using poetry to manage dependencies :

#+begin_src yaml
name: ci
on: [push]
jobs:
  ci:
    runs-on: ubuntu-latest
    steps:
      - uses: actions/checkout@v2
      - uses: actions/setup-python@v2
        with:
          python-version: 3.8
      - name: Install Poetry
        uses: snok/install-poetry@v1
        with:
          virtualenvs-create: true
          virtualenvs-in-project: true
      - name: Load cached virtual environment
        id: cached-venv
        uses: actions/cache@v2
        with:
          path: .venv
          key: venv-${{ runner.os }}-${{ hashFiles('**/poetry.lock') }}
      - name: Install dependencies
        if: steps.cached-venv.output.cache-hit != 'true'
        run: poetry install --no-interaction --no-root
      - name: Build documentation
        run: |
          source .venv/bin/activate
          cd docs
          make html
      - name: Deploy documentation
        uses: JamesIves/github-pages-deploy-action@v4.2.3
        with:
          branch: gh-pages
          folder: "docs/_build/html"
#+end_src

* Tk

** Minimal Example

#+begin_src python
import tkinter as tk

class Application:
    def __init__(self, window) -> None:
        self.window = window


window = tk.Tk()
pyrat = Application(window)
window.mainloop()
#+end_src

** Adding Widgets

#+begin_src python
import tkinter as tk
from tkinter import ttk

window = tk.Tk()
button = ttk.Button(window, text="yay")
button.grid()
window.mainloop()
#+end_src

** Adding a Menu

Inspired from [[https://tkdocs.com/tutorial/menus.html][TKDocs]] :

#+begin_src python
import tkinter as tk
from tkinter import ttk
from tkinter.constants import FALSE


def open_file():
    print("open file here...")

window = tk.Tk()

# deactive tearoff (it looks odd)
window.option_add("*tearOff", FALSE)

# main menu bar
menubar = tk.Menu(window)

# menu_file is a children of menubar
menu_file = tk.Menu(menubar)
menu_file.add_command(label="Open...", command=open_file)
menubar.add_cascade(menu=menu_file, label="File")

# declare the menubar as the window menu
window["menu"] = menubar

window.mainloop()
#+end_src

** Theming

Install the =ttkthemes= python package :

#+begin_src sh
pip install ttkthemes
#+end_src

Minimal exemple :

#+begin_src python
from ttkthemes import ThemedTk

window = ThemedTk(theme="radiance")
window.mainloop()
#+end_src

A list of possible themes can be found in the [[https://wiki.tcl-lang.org/page/List+of+ttk+Themes][tcl-lang wiki]].


** References

- [[https://tkdocs.com/tutorial/index.html][TkDocs tutorials]]

* Gtk

** Installation

Step one : install gtk devel headers through the systen package manager :

#+begin_src sh
sudo dnf install gtk3-devel
#+end_src

Step two : install python packages through pip :

#+begin_src sh
pip3 install pycairo PyGObject
#+end_src

*** References

- [[https://pygobject.readthedocs.io/en/latest/]]


** Minimal example
#+begin_src python
import gi

     gi.require_version("Gtk", "3.0")
     from gi.repository import Gtk

     class Application:

	 def __init__(self):

	     self.builder = Gtk.Builder.new_from_file("/path/to/file")

	     # assuming the main window has identifier main_window
	     self.window = self.builder..get_object("main_window")

	     self.window.show_all()


     app = Application()
     Gtk.main()
#+end_src

** Creating an overlay

Python example :

#+begin_src python
class PyratTimelineEventsOverlay(Gtk.Misc):
    """
    :todo:
    :ivar events: list of dict with keys :
        ``location``: float (0-1)
        ``name``: str
    :ivar current_pointed_event: event currently pointed by the mouse
    """

    __gtype_name__ = "PyratTimelineOverlay"

    def __init__(self) -> None:
        super().__init__()
        self.margin = 10
        self.events: List[dict] = []
        self.current_pointed_event: Optional[dict] = None
        # do not intercept events
        self.connect("realize", lambda *args: self.get_window().set_pass_through(True))

    def do_draw(self, cr: cairo.Context):
        size = self.get_allocation()

        cr.set_source_rgb(3, 119, 252)
        cr.set_line_width(2)
        for event in self.events:
            x = event["location"] * size.width
            cr.move_to(x, self.margin)
            cr.line_to(x, size.height - self.margin)
            cr.stroke()

        if not self.current_pointed_event is None:
            x = self.current_pointed_event["location"] * size.width
            cr.set_source_rgb(3, 119, 252)
            cr.move_to(x + self.margin, size.height / 3)
            cr.show_text(self.current_pointed_event["name"])

    def on_mouse_motion(
        self, widget: PyratTimelineEventsOverlay, event: Gdk.EventButton
    ):
        self.current_pointed_event = None
        if len(self.events) == 0:
            return

        size = self.get_allocation()
        y_percentage = event.y / size.height
        margin_y_percentage = self.margin / size.height
        if y_percentage < margin_y_percentage or y_percentage > 1 - margin_y_percentage:
            self.current_pointed_event = None
            self.queue_draw()

        x_percentage = event.x / size.width
        closest_event = min(
            self.events, key=lambda e: abs(e["location"] - x_percentage)
        )
        if abs(closest_event["location"] - x_percentage) < 0.01:
            self.current_pointed_event = closest_event
            self.queue_draw()


class PyratTimeline(Gtk.Overlay):

    __gtype_name__ = "PyratTimeline"

    def __init__(self) -> None:

        super().__init__()

        self.scale = Gtk.HScale(
            adjustment=Gtk.Adjustment(value=0, lower=0, upper=100, step_increment=1)
        )
        self.scale.set_draw_value(False)
        print(self.scale.get_range_rect().x)
        print(self.scale.get_range_rect().y)
        print(self.scale.get_range_rect().width)
        print(self.scale.get_range_rect().height)
        print(self.scale.get_slider_range())

        self.events_overlay = PyratTimelineEventsOverlay()

        self.add(self.scale)
        self.add_overlay(self.events_overlay)

        # set events to not be intercepted by self.events_overlay
        self.set_overlay_pass_through(self.events_overlay, True)

        self.add_events(Gdk.EventMask.POINTER_MOTION_MASK)
        self.connect("motion_notify_event", self.events_overlay.on_mouse_motion)
#+end_src

** LSP setup
Because the Gtk python packages are using introspection, typing and completion won't work out of the box. However, you can install the following package to have basic stubs :
#+begin_src sh
pip3 install pygobject-stubs
#+end_src

* Matplotlib

** Cheatsheets

[[https://matplotlib.org/cheatsheets/][All cheatsheets]]

** General structure

#+begin_src artist
   +-------------------------------------------------+
   |                                                 |
   |  +------------------+   +-------------------+   |
   |  |                  |   |                   |   |
   |  |                  |   |                   |   |
   |  | Can draw in Axes |   |                   |   |
   |  | Using coords     |   |                   |   |
   |  |                  |   |                   |   |
   |  |                  |   |                   |   |
   |  |                  |   |                   |   |
   |  |                  |   |                   |   |
   |  |        Axe 1     |   |      Axe 2        |   |
   |  +------------------+   +-------------------+   |
   |                                    Figure       |
   +-------------------------------------------------+
#+end_src

See https://matplotlib.org/stable/tutorials/introductory/usage.html#parts-of-a-figure for the full figure anatomy

** Basic object-oriented commands

*** Basic plotting

#+begin_src python
import matplotlib.pyplot as plt

fig, ax = plt.subplots() # or ax = plt.subplot()
ax.plot([1, 2, 3, 4], [2, 4, 6, 8])
plt.show()
#+end_src

Result :

#+begin_src artist
 +-----------------------------------------------------------------+
 |                     	           	               	           |
 |    +-------------------------------------------------------+    |
 |    |                                                       |    |
 |    |   |                                                   |    |
 |    | 8 |                /-                                 |    |
 |    |   |              /-                                   |    |
 |    | 6 |            /-  Line                               |    |
 |    |   |          /-                                       |    |
 |    | 4 |       /--                                         |    |
 |    |   |     /-                                            |    |
 |    | 2 |   /-                                              |    |
 |    |   | /-                Axis                            |    |
 |    |   +-1----2----3-----4----                     Axe #1  |    |
 |    +-------------------------------------------------------+    |
 |                                                  Figure         |
 +-----------------------------------------------------------------+

#+end_src


This also works with strings as X :

#+begin_src python
import matplotlib.pyplot as plt

fig, ax = plt.subplots()
ax.plot(["label1", "label2", "label3", "label4"], [2, 4, 6, 8])
plt.show()
#+end_src

or nothing as X (implicitly : ~list(range(len(y)))~) :

#+begin_src python
import matplotlib.pyplot as plt

fig, ax = plt.subplots()
ax.plot([2, 4, 6, 8])
plt.show()
#+end_src

*** Multiples axes

Making a 2 by 2 grid :

#+begin_src python
import matplotlib.pyplot as plt

fig, axs = plt.subplots(2, 2)
#+end_src

~axs~ is then an array you can use to plot :

#+begin_src python
axs[0][0].plot([1, 2, 3])
axs[0][1].plot([1, 2, 3])
axs[1][0].plot([1, 2, 3])
axs[1][1].plot([1, 2, 3])
#+end_src

*** Useful axes function

#+begin_src python
ax.set_title("title")
ax.set_xlabel("X Axis label")
ax.set_ylabel("Y Axis label")
ax.legend(["item 1 legend", "item 2 legend"])
#+end_src

** Different plots

*** Histogram

[[https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.hist.html#matplotlib.pyplot.hist][Reference]]

#+begin_src python
X = [0, 0, 1, 2, 0, 1, 0]

fig, ax = plt.subplots()
ax.hist(X)
plt.show()
#+end_src


#+begin_src artist
   +------------------------------------------------+
   |                                                |
   |  |                                             |
   |  |                                             |
   |  +-----------+                                 |
   |  |  .... ... |                                 |
   |  | ........  |                                 |
   |  |.........  |                                 |
   |  |  .......  |                                 |
   |  | ....... . +----------+                      |
   |  |.......... |..... ..  |                      |
   |  |  .........|  ..... . |                      |
   |  |  ... .... | ........ +------------+         |
   |  | . . . .   |  .. . .  | . ... . . .|         |
   |  +----0------+----1-----+------2-----+---      |
   +------------------------------------------------+
#+end_src

*** Scatter plots

#+begin_src python
X = [1, 2, 3]
Y = [10, 12, 20]

fig, ax = plt.subplots()
ax.scatter(X, Y)
plt.show()
#+end_src

#+begin_src artist
   +-------------------------------------------+
   |                                           |
   |   |                                       |
   |   |                                       |
   |   |                                       |
   |   |               o                       |
   |   |                                       |
   |   |          o                            |
   |   |    o                                  |
   |   |                                       |
   |   +--------------------------------       |
   |        1     2     3                      |
   +-------------------------------------------+
#+end_src

*** Bar plots

[[https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.bar.html][Reference]]

#+begin_src python
y = [8, 4, 2]
X = ["group 1", "group 2", "group 3"]

fig, ax = plt.subplots()
ax.bar(X, y)
plt.show()
#+end_src

#+begin_src artist
   +------------------------------------------------+
   |                                                |
   |  |                                             |
   |  |                                             |
   |  +-----------+                                 |
   |  |  .... ... |                                 |
   |  | ........  |                                 |
   |  |.........  |                                 |
   |  |  .......  |                                 |
   |  | ....... . +----------+                      |
   |  |.......... |..... ..  |                      |
   |  |  .........|  ..... . |                      |
   |  |  ... .... | ........ +------------+         |
   |  | . . . .   |  .. . .  | . ... . . .|         |
   |  +--group-0--+--group-1-+---group-2--+---      |
   +------------------------------------------------+
#+end_src

**** Setting the bar labels angle

By default, bar labels are horizontal. This can be an issue when
labels are bigger than the width of the bars, as they will overlap. A
solution is to turn the label to a certain angle, but it is not
straightforward. Use the following recipe for easy results:

#+begin_src python
y = [8, 4, 2]
X = ["group 1", "group 2", "group 3"]

fig, ax = plt.subplots()
ax.bar(X, y)
plt.xticks(rotation=30, ha="right")
plt.show()
#+end_src

This, however, wont work in the case of subplots since the ~xticks~
function only applies to the current ~ax~. In that case, use:

#+begin_src python
y = [8, 4, 2]
X = ["group 1", "group 2", "group 3"]

fig, axs = plt.subplots(1, 2)

for ax in axs:
    ax.bar(X, y)

for ax in axs:
    for label in ax.get_xticklabels():
        label.set_rotation(30)
        label.set_ha("right")

plt.show()
#+end_src

For more details, see [[https://www.pythoncharts.com/matplotlib/rotating-axis-labels/][this article]].

** Animation

The most convenient class seems to be the ~FuncAnimation~ class.

Interesting ~FuncAnimation~ parameters are :

- ~frames~ : the values used to call the ~update~ function
- ~interval~ : the time interval, in ms, between two function calls

*** Animating a plot

Here is an example of a simple sin wave animation :

#+begin_src python
import matplotlib.pyplot as plt
import matplotlib.animation as mpla
import numpy as np

fig, ax = plt.subplots()
ax.set_xlim(0, 100)
ax.set_ylim(-5, 5)

xdata, ydata = [], []
line, = plt.plot([], [])

def update(i):
    xdata.append(i)
    ydata.append(np.sin(i))
    line.set_data(xdata, ydata)
    return line

anim = mpla.FuncAnimation(fig, update, frames=np.arange(0, 100, 0.1), interval=50)
plt.show()
#+end_src

#+RESULTS:
: None

*** Animating a scatter plot

Because of course it has to be different...

#+begin_src python
import matplotlib.pyplot as plt
import matplotlib.animation as mpla

fig, ax = plt.subplots()
ax.set_xlim(0, 10)
ax.set_ylim(0, 10)

scatter = ax.scatter([], [])

def update(i):
    scatter.set_offsets([[i, i]])
    return scatter,

anim = mpla.FuncAnimation(fig, update, frames=range(10))
plt.show()
#+end_src

#+RESULTS:
: None

*** Animating patches

Matplotlib =patches= can also be animated, provided they are in a
matplotlib =collection=. Here is an example moving a rectangle from
left to right :

#+begin_src python
import matplotlib.animation as mpla
import matplotlib.collections as clt
from matplotlib.patches import Rectangle
import matplotlib.pyplot as plt

fig, ax = plt.subplots()
ax.set_xlim(0, 20)
ax.set_ylim(-10, 10)

patch_collection = clt.PatchCollection([Rectangle((0, 0), 1, 1)])
ax.add_collection(patch_collection)

def update(i):
    patch_collection.set_paths([Rectangle((i, 0), 1, 1)])

anim = mpla.FuncAnimation(fig, update, frames=range(20))
plt.show()
#+end_src

#+RESULTS:
: None

** Font size

from [[https://stackoverflow.com/questions/3899980/how-to-change-the-font-size-on-a-matplotlib-plot/39566040#39566040][this answer on stackoverflow]] :

#+begin_src python
import matplotlib.pyplot as plt

SMALL_SIZE = 8
MEDIUM_SIZE = 10
BIGGER_SIZE = 12

plt.rc('font'  , size=SMALL_SIZE)       # controls default text sizes
plt.rc('axes'  , titlesize=SMALL_SIZE)  # fontsize of the axes title
plt.rc('axes'  , labelsize=MEDIUM_SIZE) # fontsize of the x and y labels
plt.rc('xtick' , labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('ytick' , labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)   # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE) # fontsize of the figure title
#+end_src

However, I found this to be unreliable sometimes... Another solution
(that works !) is to use the ~fontsize~ argument in fonctions dealing
with text:

#+begin_src python
import matplotlib.pyplot as plt

fig, ax = plt.subplot()
plt.plot([1,2,3,4,5])
plt.ylabel("custom", fontsize=20)
plt.show()
#+end_src

** Figure size

The figure size can be set in inches:

#+begin_src python
fig, ax = plt.subplot()
fig.set_size_inches(10, 4)
#+end_src

An inch is 2.54 cm.

** Legend

[[https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.legend.html][Legend documentation]]

a legend can be put on an axis :

#+begin_src python
ax.legend(["precision", "recall"])
#+end_src

*** Custom positioning

it is possible to control the legend's positioning using a combination
of ~loc~ and the ~bbox_to_anchor~ tuple :

#+begin_src python
ax.legend(["precision", "recall"], loc="center", bbox_to_anchor=(0.5, 0.5))
#+end_src

The ~bbox_to_anchor~ tuple has the following form : ~(x, y)~, all of
these numbers being floats between 0 and 1. ~x~ and ~y~ are the
coordinates of the legend's point defined by ~loc~ in the coordinates
of the current ax. So in the previous example, the /center/ of the
legend would be placed at the center of the current ax. When no ~loc~
is specified, the top left point of the legend is used for
positioning.

** Ticks

*** Force ticks to be integers

#+begin_src python
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

fig, ax = plt.subplots()
#...
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
#+end_src

