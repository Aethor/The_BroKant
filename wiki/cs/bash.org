# -*- org-hide-emphasis-markers: t; -*-
#+TITLE: Bash
#+AUTHOR: Arthur Amalvy


* Bash arrays

** Syntax cheat sheet

| Syntax          | Description                             | Python equivalent               |
|-----------------+-----------------------------------------+---------------------------------|
| ~array=(1 2 3)~   | Create an array                         | ~array = [1, 2, 3]~               |
| ~${array[2]}~     | Access to array third element           | ~array[2]~                        |
| ~${array[@]}~     | Access to all elements                  | ~array~                           |
| ~${!array[@]}~    | Access array indices                    | ~range(len(array))~               |
| ~${#array[@]}~    | Access array size                       | ~len(array)~                      |
| ~array[2]=4~      | Set array third element to 4            | ~array[2] = 4~                    |
| ~array+=(4)~      | Append 4 to array                       | ~array.append(4)~ or ~array += [4]~ |
| ~${array[@]:1:2}~ | Retrieve 2 elements starting at index 1 | ~array[1:3]~                      |
