#+TITLE: Parsing
#+AUTHOR: Arthur Amalvy


* LR Parsers

A LR parser is a bottom-up parser that uses a stack. Given an input
string, it reads it left-to-right, creating a parse tree by either
performing a =shift= or a =reduce= action:

- =shift= puts the next token on the stack. This creates a new tree
  leaf node.
- =reduce= transform a series of symbols in the stack to produce a new
  symbol according to a production rule. It can possibly link two tree
  branchs.


Example from [[https://en.wikipedia.org/wiki/LR_parser][wikipedia]][fn:: note that production rules are used in
reverse: this is expected]:

| Step | Stack            | Unparsed  | Shift/Reduce                |
|------+------------------+-----------+-----------------------------|
|    0 |                  | A * 2 + 1 | shift                       |
|    1 | id               | * 2 + 1   | Value → id                  |
|    2 | Value            | * 2 + 1   | Products → Value            |
|    3 | Products         | * 2 + 1   | shift                       |
|    4 | Products *       | 2 + 1     | shift                       |
|    5 | Products * int   | + 1       | Value → int                 |
|    6 | Products * Value | + 1       | Products → Products * Value |
|    7 | Products         | + 1       | Sums → Products             |
|    8 | Sums             | + 1       | shift                       |
|    9 | Sums +           | 1         | shift                       |
|   10 | Sums + int       | eof       | Value → int                 |
|   11 | Sums + Value     | eof       | Products → Value            |
|   12 | Sums + Products  | eof       | Sums → Sums + Products      |
|   13 | Sums             | eof       |                             |

The produced tree is as follows:

#+begin_src text
Sums + -- Products -- Value -- int (1)
     |
     + -- (+)
     |
     + -- Sums -- Products + -- Value -- int (2)
                           |
                           + -- (*)
                           |
                           + -- Products -- Value -- id (A)
#+end_src

LR parsers are usually designed as $LR(k)$, $k$ being the number of
tokens the parser is allowed to look ahead. Most LR parsers being
$LR(1)$, these are simply designed LR.
