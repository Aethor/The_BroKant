#+TITLE: Arduino
#+AUTHOR: Arthur Amalvy


* Command line setup

** PlatformIO

[[https://docs.platformio.org/en/latest/core/quickstart.html][Documentation]]

*** Install

Easy install (Linux):

#+begin_src sh
curl -fsSL -o get-platformio.py https://raw.githubusercontent.com/platformio/platformio-core-installer/master/get-platformio.py
python3 get-platformio.py
#+end_src

Next, add =~/.platformio/penv/bin= to your =PATH=.

*** Basics

One can create a project using ~pio project init --board ${ID}~. To
find the =ID= of the board, see ~pio boards list~.

After creation, the =platformio.ini= file contains the project
configuration.

*** Emacs integration

When creating a project, use ~pio project init --ide emacs --board ${ID}~.

There is =platformio-mode=. Commands:

| Function                | Default Keymap |
|-------------------------+----------------|
| Build                   | =C-c i b=      |
| Upload                  | =C-c i u=      |
| Upload using programmer | =C-c i p=      |
| Upload SPIFFS           | =C-c i s=      |
| Monitor device          | =C-c i m=      |
| Clean                   | =C-c i c=      |
| Update                  | =C-c i d=      |
| Update workspace        | =C-c i i=      |
| Boards List             | =C-c i l=      |


** Arduino-mk

/note: platformIO is a newer (probably better) alternative/

See [[https://github.com/sudar/Arduino-Makefile][Arduino-mk]]. For Fedora, there are [[https://github.com/sudar/Arduino-Makefile/tree/master/packaging/fedora][some install instructions]] that
necessitate to manually build a RPM. There are some issues: the
instructions are incomplete, and =arduino-core= is a required
dependency, but does not exist as of right now (tested on Fedora 37 -
current Fedora is 39). The following script should install
=arduino=mk= correctly:

#+begin_src sh
sudo dnf install rpmdevtools
git archive HEAD --prefix=arduino-mk-1.6.0/ -o ../arduino-mk-1.6.0.tar.gz
mkdir -p ~/rpmbuild/{SOURCES,SPECS}
cp ../arduino-mk-1.6.0.tar.gz ~/rpmbuild/SOURCES/
cp packaging/fedora/arduino-mk.spec ~/rpmbuild/SPECS/
cd ~/rpmbuild/SPECS/
# fix phantom arduino-core dependency
sed -i 's/arduino-core //g' arduino-mk.spec
rpmbuild -ba arduino-mk.spec
sudo dnf install ~/rpmbuild/RPMS/noarch/arduino-mk-1.6.0.fc37.noarch.rpm
#+end_src
