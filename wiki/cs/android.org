#+TITLE: Android Development
#+AUTHOR: Arthur Amalvy


* Project File Hierarchy

- =/manifests/AndroidManifest.xml= :: XML file with global application
  infos (app description, permissions, main activity...)
- =/java= :: source code
- =/res= :: resources files
  - =/res/mipmap= :: app launch icons
  - =/res/drawable= :: other images
  - =/res/layout= :: layout template files
  - =/res/values= :: key-value pairs that can be referenced in the
    source code
  - =/res/themes= :: themes and styles
- =/build.gradle= :: build instructions

  
* Main Structures

** Activity

An activity is usually an unique screen. The user is expected to
switch between activities.

Basic example:

#+begin_src java
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_main);
    }
}
#+end_src

*** ActivityManager

** Intents

An intent is a message sent between components of an app. An intent
can start an activity, start or stop a service, send a broadcast...

An intent can be:

- explicit :: in that case, it specifies the target component
- implicit :: in that case, it specifies only the type of the target
  component

** Broadcast Receivers

Broadcast Receivers are subscribed to sources and can execute code
when these sources send new events.

** Services

** Content Providers

Content providers provide datas to different apps.

Some content providers:

- Settings Provider :: provides system parameters to apps
- Media Store :: provides common storage for medias (pictures,
  musics...)
- Contact Providers :: provides phone contacts to apps

** Context

*** Activity / Service Context

~Activity~ and ~Service~ inherit from ~Context~.

ex: ~this.getResources()~ *TODO*

*** Application Context

#+begin_src java
Context.getApplicationContext();
#+end_src

** Layout

Usually described by resources.

Types of layouts:

- FrameLayout ::
- LinearLayout :: align elements in a direction (horizontal or vertical)
- TableLayout ::
- RelativeLayout ::
- ConstraintLayout :: 

* Accessing Resources

A global variable named =R= can be used to access resources. Usually,
getting a resource has the following form:
~R.resource_type.resource_name~. Example:

#+begin_src java
setContentView(R.layout.activity_main);
#+end_src
