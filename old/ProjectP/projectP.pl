#!/bin/perl
use warnings;
use File::Copy;

#project:Project : a project template creator

$version=0.01;


print "project:Project v$version\n\n";

print "please supply the path to your project\n";
chomp($path=<STDIN>);
unless(mkdir($path) && mkdir($path . "/src")){
	die "Error : could not create directory.\n";
}

print "Please indicate the type of your project :\n* cpp : create c++ project\n* c : create c project\n* upc : create upc project\n";
chomp($type=<STDIN>);




if($type eq "cpp"){
	print "Creating C++ project\n";
	$CC = 'g++';
	$FILES = '*.cpp *.cc';
}
elsif($type eq "c"){
	print "Creating C project\n";
	$CC = 'gcc';
	$FILES = '*.c';
}
elsif($type eq "upc"){
	print "Creating upc project\n";
	$CC = 'upcc';
	$FILES = '*.upc';
	$THREAD = '-T 4';
}
else{
	die "Error : impossible to determine project type.";
}

copy('template', $path . 'template') or die "Error generating Makefile.\n";

open MAKEFILE, $path . 'template';

while(<MAKEFILE>){
	s/^CC=/CC=$CC/g;
	s/^FILES=/FILES=$FILES/g;
	s/^THREAD=/THREAD=$THREAD/g;
}
