" Vim syntax file
" Language : Standard Gaud Algorithm
" Maintainer : Arthur Amalvy / Aethor 
" Latest Revision : 19/10/2016

if exists("b:current_syntax")
	finish
endif


" Keywords


syn keyword algoKeywords if else end 
syn keyword algoKeywords for while do repeat until
syn keyword algoKeywords return
highlight link algoKeywords Keyword


syn keyword algoFunction print insertHead insertTail removeHead removeTail
highlight link algoFunction Function


syn keyword algoType integer float character 
highlight link algoType Type

syn keyword algoProgramme Function 
highlight link algoProgramme Special


syntax region algoString start=/\v"/ skip=/\v\\./ end=/\v"/
highlight link algoString String


syntax match algoOperator "\v\*"
syntax match algoOperator "\v\*"
syntax match algoOperator "\v/"
syntax match algoOperator "\v\+"
syntax match algoOperator "\v-"
syntax match algoOperator "\v\?"
syntax match algoOperator "\v\*\="
syntax match algoOperator "\v/\="
syntax match algoOperator "\v\+\="
syntax match algoOperator "\v-\="

highlight link algoOperator Operator



