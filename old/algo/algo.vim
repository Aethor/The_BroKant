" Vim syntax file
" Language : Standard Gillou Algorithm
" Maintainer : Arthur Amalvy / Aethor 
" Latest Revision : 27/04/2016

if exists("b:current_syntax")
	finish
endif


" Keywords


syn keyword algoKeywords Si Sinon Fin Début Choisir Parmi
syn keyword algoKeywords Pour Tant Que Faire Répéter
syn keyword algoKeywords Retourner
highlight link algoKeywords Keyword


syn keyword algoFunction Ecrire ecrire Lire ecrire Fonctions fonctions Variable variable Variables variables Structure structure 
highlight link algoFunction Function


syn keyword algoType Entier Réel entier réel caractère Caractère rien Rien
highlight link algoType Type

syn keyword algoProgramme Programme programme fonction Fonction #
highlight link algoProgramme Special


syntax region algoString start=/\v"/ skip=/\v\\./ end=/\v"/
highlight link algoString String


syntax match algoOperator "\v\*"
syntax match algoOperator "\v\*"
syntax match algoOperator "\v/"
syntax match algoOperator "\v\+"
syntax match algoOperator "\v-"
syntax match algoOperator "\v\?"
syntax match algoOperator "\v\*\="
syntax match algoOperator "\v/\="
syntax match algoOperator "\v\+\="
syntax match algoOperator "\v-\="

highlight link algoOperator Operator



