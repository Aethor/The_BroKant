#!/bin/bash

# created by Arthur Amalvy
# this code is licensed under the terms of the GNU GPLv3

# args : a directory to look inside it (dir), and a subdirectory name
# (subdir). if subdir is found in dir, will delete all files in
# subdir. Additionaly, will launch itself in every directory in dir
# which is not subdir.
function deleteFiles () {

    local dir=$1
    dir="${dir%/}" #skip trailing /
    subdir=$2
    subdir="${subdir%/}"

    for localDir in $(ls $dir); do

	if [[ ! -d "${dir}/${localDir}" ]]; then
	    continue
	fi

	if [[ ! "${subdir}" == ${localDir##*/} ]]; then # check if this is a matching dir or not
	    deleteFiles "${dir}/${localDir}" "${subdir}"
	    continue
	fi

	if [[ -z $(ls -A "${dir}/${localDir}") ]]; then # check if this matching dir is empty
	    printf "found matching directory ${dir}/${localDir}/, but it was empty.\n\n"
	    continue
	fi

	echo "found files in ${dir}/${localDir}/ : "
	ls -lh "${dir}/${localDir}/"
	read -p "do you want to suppress these files in ${dir}/${localDir}/ ? [default : yes][y(es)/n(o)] " answer

	if [[ -z $answer || $answer =~ "y" ]]; then

	    oldIFS="$IFS"
	    IFS="" # avoid splitting on spaces
	    
	    for file in ${dir}/${localDir}/*; do

		local fileSize=$(wc -c < $file)
		rm -r $file
		if [[ $? -eq 0 ]]; then
		    totalFilesDeleted=$((totalFilesDeleted + 1))
		    totalSize=$((totalSize + fileSize))
		    echo "deleted file $file"
		fi
		
	    done

	    # restore IFS
	    IFS="$oldIFS"
	    echo

	else
	    printf "did not delete files in ${dir}/${localDir}/\n\n"
	fi
	   
    done
}

targetDir="dead_sounds"
targetDir2="dead"
totalSize=0
totalFilesDeleted=0


if [[ $# -eq 0 ]]; then
    echo 'syntax : absurage [directories..]'
    exit 1
fi


for directory in $@; do

    if [[ ! -d "${directory}" ]]; then
	continue
    fi

    read -p "absurage will delete every file, recursively, in specified subdirectory in directory $directory. choose subdirectory [default : ${targetDir} and ${targetDir2}] : " answer
    if [[ ! -z "${answer}" ]]; then
	deleteFiles "${directory}" "${answer}"
    else
	deleteFiles "${directory}" "${targetDir}"
	deleteFiles "${directory}" "${targetDir2}"
    fi

done


if [[ -x "$(command -v numfmt)" ]]; then
    formatted=$(numfmt --to=si ${totalSize})
    # no difference between formatted output and input: output is in bytes
    if [[ "${formatted}" -eq "${totalSize}" ]]; then
	formatted="${formatted} bytes"
    fi
    printf "no more files to delete !\n deleted a total of $totalFilesDeleted files, freeing a storage of $formatted.\n"
else
    printf "no more files to delete !\n deleted a total of $totalFilesDeleted files, freeing a storage of $totalSize bytes.\n"
    echo "tip : install 'numfmt' so that absurage.sh can display the freed storage with human readable units."
fi

