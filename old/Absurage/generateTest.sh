#!/bin/bash

dir1="test/dead"
dir2="test/dead_sounds"
dir3="test/someDir"

file1="ayo.txt"
file2="ay i.txt"
file3="e,rze.txt"


mkdir test
mkdir $dir1 $dir2 $dir3

for dir in $dir1 $dir2 $dir3; do
    for file in $file1 $file2 $file3; do
	touch ${dir}/${file}
	echo something something > ${dir}/${file}
    done
done

