#!/bin/bash

#This script was made by Arthur Amalvy

source="$1"
target="$2"
red="#FF1100"
blue="#37B6CE"
orange="#F38E00"
yellow="#F3DF00"
green="#32D92A"
mfire="#CE004F"



#Initialisation du fichier
cat "$source" > "$target"


#Symbole à changer avant le HTML
sed -i -e "s/->/<span style=color:$red>-><\/span>/g" $target


#Suppression des ;
sed -i -e 's/;//g' $target

#Suppression des commentaires et des direcives de préprocesseurs
sed -i -e '/^#/d' $target
sed -i -e'/\/\*/,/\*\//d' $target
sed -i -e '/\/\/.*/d' $target


#suppression du return 0 et transformation des autres return;
sed -i -e 's/return 0//g' $target
sed -i -e 's/return/retourner/g' $target


#Sauts de lignes
#sed -i ':z;N;s/\n\n/\n<br \/>/;bz' $target 
sed -i ':a;N;$!ba;s/\n/<br \/>\n/g' $target #wut


#Suppression des accolades
sed -i -e 's/{/<br \/>/g' $target
sed -i -e 's/}/<\/blockquote>Fin<br \/>/g' $target


#Programme ou fonction ?
if [ $1 = "main.c" ] || [ $1 = "./main.c" ]
then

	sed -i -e 's/int main()/<h1>Programme<\/h1>\n\n<h2>fonctions<\/h2>\n\n<h2>variables<\/h2><p>/g' $target

else
	
	sed -i "1i <h1>Fonction<\/h1>\n\n<h2>fonctions<\/h2>\n\n<h2>variables<\/h2><p>" $target

fi

#Fonctions
sed -i -e "s/\([^\t ]*(\)/<span style=color:$mfire>\1<\/span>/g" $target


#Fonctions utilisées
sed -i -e 's/#include .*//g' $target
sed -i -e 's/#include .*//g' $target
sed -i -e 's/\.h.//g' $target


#Variables
sed -i -e "s/\<int\>/<span style=color:$orange>entier <\/span>/g" $target
sed -i -e "s/\<float\>/<span style=color:$orange>réel <\/span>/g" $target
sed -i -e "s/\<char\>/<span style=color:$orange>caractère <\/span>/g" $target
sed -i -e "s/\<bool\>/<span style=color:$orange>booléen <\/span>/g" $target
sed -i -e "s/\<S_list\>/<span style=color:$orange>S_list<\/span>/g" $target
sed -i -e "s/\<S_element\>/<span style=color:$orange>S_element<\/span>/g" $target
sed -i -e "s/\<S_status\>/<span style=color:$orange>S_status<\/span>/g" $target
sed -i -e "s/\<S_wrestler\>/<span style=color:$orange>S_wrestler<\/span>/g" $target
sed -i -e "s/\<S_gfile\>/<span style=color:$orange>S_gfile<\/span>/g" $target


#Symboles
sed -i -e "s/ = /<span style=color:$red> <-- <\/span>/g" $target
sed -i -e "s/==/<span style=color:$red> = <\/span>/g" $target
sed -i -e "s/!=/<span style=color:$red>!=<\/span>/g" $target
sed -i -e "s/\<0\>/<span style=color:$green>0<\/span>/g" $target
sed -i -e "s/+/<span style=color:$red>+<\/span>/g" $target
sed -i -e "s/-/<span style=color:$red>-<\/span>/g" $target
sed -i -e "s/\*/<span style=color:$red>*<\/span>/g" $target
sed -i -e "s/\<NULL\>/<span style=color:$green>NULL<\/span>/g" $target
sed -i -e "s/\<true\>/<span style=color:$green>Vrai<\/span>/g" $target
sed -i -e "s/\<false\>/<span style=color:$green>Faux<\/span>/g" $target
sed -i -e "s/&&/<span style=color:$red>ET<\/span>/g" $target
sed -i -e "s/||/<span style=color:$red>||<\/span>/g" $target
sed -i -e "s/(/<span style=color:$mfire>(<\/span>/g" $target
sed -i -e "s/)/<span style=color:$mfire>)<\/span>/g" $target



#Structures
sed -i -e "s/\<while\>/<span style=color:$yellow>Tant Que <\/span><blockquote>/g" $target
sed -i -e "s/else if\(.*\)/<blockquote><span style=color:$yellow>Sinon Si <\/span>\1<blockquote>/g" $target
sed -i -e "s/\<if\>\(.*\)/<span style=color:$yellow>Si<\/span>\1<blockquote>/g" $target
sed -i -e "s/\<else\>/<blockquote><span style=color:$yellow>Sinon <\/span><blockquote>/g" $target
sed -i -e "s/\<Fin\>/<span style=color:$yellow>Fin<\/span><\/blockquote>/g" $target


#Coloration des chaines de caractères
sed -i -re "s/(\".*\")/<span style=color:$blue>\1<\/span>/g" $target


#Structure HTML GG
sed -i "1i<!DOCTYPE html>\n<html>\n<head>\n<meta charset="utf-8">\n<title>$target</title>\n</head>\n<body style=background:#272822>\n<span style="color:#FFFFFF">\n" $target


#Fin de structure HTML
echo -e "</span>\n</p>\n</body>\n</html>" >> $target

echo -e "Spillow.sh : new file $target is ready !\n\n"

																	
