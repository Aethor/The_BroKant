" Vim syntax file
" Language :    Markdown Lombardi (with basic LaTeX support)
" Maintainer :  Arthur "Aethor" Amalvy
" Last Change : 2016 Oct 12

if exists("b:current_syntax")
	finish
endif

let b:current_syntax = "mdlbi"

" --- Markdown Lombardi ---

"Markdown Lombardi few keywords
syntax match mdlbiKeyword "\v!picture"
syntax match mdlbiKeyword "\v!video"
syntax match mdlbiKeyword "\v!title"
syntax match mdlbiKeyword "\v!author"
syntax match mdlbiKeyword "\v!date"
syntax match mdlbiKeyword "\v!tableOfContents"
highlight link mdlbiKeyword Keyword


"Markdown Lombardi lists
syntax match mdlbiList "\v^\*"
syntax match mdlbiList "\v^\t\*"
syntax match mdlbiList "\v^\t\t\*"
highlight link mdlbiList PreCondit


"Markdown Lombardi titles
syntax match mdlbiTitle "\v^#.*$"
syntax match mdlbiTitle "\v^[0-9]+# .*$"
highlight link mdlbiTitle Function


"Markdown Lombardi : Bold, Italics, Code
syntax match mdlbiEmbed "\v_\>.*\<_"
syntax match mdlbiEmbed "\v\>\>.*\<\<"
syntax match mdlbiEmbed "\v\/\>.*\<\/"
highlight link mdlbiEmbed String


"Markdown Lombardi : comments
syntax match mdlbiComment "\v\~\~.*$"
highlight link mdlbiComment Comment


"--- LaTeX ---

"LaTeX keywords
syntax match texKeyword "\v\$"
highlight link texKeyword Keyword


"LaTex operations
syntax match texOperator "\v\\[^{ \$]*"
highlight link texOperator Function

"LaTeX region : same
