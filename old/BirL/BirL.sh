#!/bin/bash


# BirL
# ----


version=1.0
source="$1"
target="$2.tex"


echo -e  "--- BirL $version ---\n\n"


#Fichier Tex basique
#la classe de document
echo -e "\\\documentclass{article}" >> $target
#package LaTeX babel (affichage table des matières, etc...)
echo -e "\\\usepackage[francais]{babel}" >> $target
echo -e "\\\usepackage[utf8]{inputenc}" >> $target
echo -e "\\\usepackage[T1]{fontenc}" >> $target
echo -e "\\\usepackage{graphicx}" >> $target

#Début du document
echo -e "\\\begin{document}" >> $target


#Insertion du titre, auteur, date...
sed -n "1p" $source >> $target
sed -n "2p" $source >> $target
sed -i -e "s/^!title(\(.*\))/\\\title{\1}/" $target
sed -i -e "s/^!author(\(.*\))/\\\author{\1}/" $target


#Affichage du titre
echo -e "\\\maketitle" >> $target


#Table des matières
grep '!tableOfContents()' $target

if [ $? -eq 0 ]

then 
	echo "$?"
	echo -e "\\\renewcommand{\\\contentsname}{Table des matières}" >> $target
	echo -e "\\\tableofcontents" >> $target

fi


#Sauts de ligne
#sed -i ':a;N;$!ba;s/\n\n/\\\vspace{0.5cm}/g' $source


#initialisation du fichier
cat "$source" >> "$target"


#fin du document
echo -e "\\\end{document}" >> $target


#Suppression : titre, auteur, date, option pour la table des matières, commentaires
sed -i -e "/!title/d" $target
sed -i -e "/!author/d" $target
sed -i -e "/!date/d" $target
sed -i -e "/!tableOfContents()/d" $target
sed -i -e "/\/\/.*$/d" $target


#transformation sections markdown -> LaTeX
sed -i -e "s/^1###\(.*\)/\\\subsubsection{\1}/" $target
sed -i -e "s/^1##\(.*\)/\\\subsection{\1}/" $target
sed -i -e "s/^1#\(.*\)/\\\section{\1}/" $target
sed -i -e "s/^###\(.*\)/\\\subsubsection*{\1}/" $target
sed -i -e "s/^##\(.*\)/\\\subsection*{\1}/" $target
sed -i -e "s/^#\(.*\)/\\\section*{\1}/" $target


#transformation des * en liste -
#début de bloc
#sed -i ':a;N;$!ba;s/\n\n\*/\n\n\\\begin{itemize}\n\*/g' $target #wut
#fin de bloc
#sed -i ':a;N;$!ba;s/\(\*.*\)\n[^*]/\1\n\\\end{itemize}\n\n/g' $target #wut again
#* --> -
#sed -i -e "s/^\*\(.*\)/\\\item \1/" $target
sed -i -e "s/^\*\(.*\)/\\\begin{itemize}\n\\\item\1\n\\\end{itemize}/g" $target
sed -i -e "s/^\t\*\(.*\)/\\\begin{itemize}\\\begin{itemize}\n\\\item\1\n\\\end{itemize}\\\end{itemize}/g" $target
sed -i -e "s/^\t\t\*\(.*\)/\\\begin{itemize}\\\begin{itemize}\\\begin{itemize}\n\\\item\1\n\\\end{itemize}\\\end{itemize}\\\end{itemize}/g" $target

#Mise en forme
sed -i -e "s/_>\(.*\)<_/\\\textbf{\1}/g" $target #Gras
sed -i -e "s/\/>\(.*\)<\//\\\textit{\1}/g" $target #Italique
sed -i -e "s/>>\(.*\)<</\\\texttt{\1}/g" $target #Code (machine à écrire, to be modified)


pdflatex "$target"

if [ "$3" != "-d" ]
then
	rm -rf "$2.tex"
	rm -rf "$2.toc"
	rm -rf "$2.aux"
	rm -rf "$2.log"
fi


echo "your file $2.pdf is ready !"
